Zolatech
========

## Overview
Implementation of Invoice API for Zola Home assignment

## Metrics

Prometheus (https://prometheus.io) metrics are accessible through http://localhost:9090/prometheus

## How-To run 
#### Maven:
- mvn spring-boot:run

#### Docker:
- mvn package 
- docker build . -t zolatech:latest
- docker run -p 9000:9000 -p 9090:9090 -t zolatech:latest

#### Kubernetes
- Out of scope ;)


## How to use the api
### Postman
- Download postman from https://www.getpostman.com/apps, load "Zolatech.postman_collection.json" and run/update the requests.

### curl
* Create invoice: 
  * curl -H 'Content-Type: application/json' http://127.0.0.1:9000/v1/invoices -d \
 '{
  
          "invoice_number": "iabcde12345",
          "po_number": "pabcde12345",
          "due_date": "2019-01-01",
          "amount_cents": 10000
  }' 

* List all invoices
  * curl http://127.0.0.1:9000/v1/invoices

* Search invoices by invoice_number (Paged)
  * curl http://127.0.0.1:9000/v1/invoices?invoice_number=a&offset=2&limit=2

* Search invoices by po_number
  * curl http://127.0.0.1:9000/v1/invoices?po_number=a
  
* Fetch Prometheus metrics
  * curl http://127.0.0.1:9090/prometheus  


---
###Invoice API Swagger style

**Version:** 1.0


### v1/invoices
---
##### ***POST***
**Summary:** Invoice creation

**Description:** Create an invoice

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| Content-Type | header |  | Yes (application/json) | string |
| Body | body |  | Yes | [CreateInvoiceRequest](#createInvoicerequest) |

**Responses**

| Code | Description |
| ---- | ----------- |
| 200  | Invoice successfully created|
| 400  | If any required information is missing or invalid|
| 500  | Internal Server error| 

##### ***GET***
**Summary:** Search for invoices

**Description:** Search invoices by invoice_number OR po_number. (Paging supported)

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| invoice_number | query |  | No | string |
| po_number | query |  | No | string |
| offset | query |  | No | integer |
| limit | query |  | No (default/max 1000) | integer |
| Accept | header |  | Yes | string |

**Responses**

| Code | Description |
| ---- | ----------- |
| 200 | Search succeed|
| 206 | If limit specified is too big and server couldn't returns all elements|
| 400 | If invalid search parameter specified |
| 500 | Internal Server error|
  

### Models
---

### CreateInvoiceRequest  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| invoice_number | string | Invoice number | Yes |
| po_number | string | Purchase order number | Yes |
| due_date | date (format: YYYY-MM-DD) | Due Date | Yes |
| amount_cents | integer | Amount in cents | Yes |
