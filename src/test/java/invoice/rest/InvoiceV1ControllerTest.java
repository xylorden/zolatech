package invoice.rest;

import static invoice.rest.InvoiceV1Controller.DEFAULT_SORT;
import static invoice.rest.InvoiceV1Controller.MAX_PAGE_SIZE;
import static io.restassured.module.webtestclient.RestAssuredWebTestClient.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.SimpleDateFormat;
import java.util.Collections;

import org.assertj.core.util.Lists;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import invoice.persistence.Invoice;
import invoice.service.InvoiceService;
import invoice.service.OffsetBasedPageable;

public class InvoiceV1ControllerTest {
	@Mock
	private InvoiceService invoiceService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void invoice_create() throws Exception {
		InvoiceV1Controller.InvoicePayload invoice = new InvoiceV1Controller.InvoicePayload();
		invoice.setNumber("ABCDE12345");
		invoice.setDueDate(new SimpleDateFormat("YYYY-MM-DD").parse("2018-01-30"));
		invoice.setAmountCents(12345);
		invoice.setPurchaseOrderNumber("X1X1X1");

		when(invoiceService.createInvoice(any(Invoice.class))).thenReturn(
				Invoice.builder()
						.id(1L)
						.dueDate(invoice.dueDate)
						.invoiceNumber(invoice.number)
						.purchaseOrderNumber(invoice.purchaseOrderNumber)
						.amountCents(invoice.amountCents)
						.createdAt(new SimpleDateFormat("YYYY-MM-DD").parse("2018-02-22"))
						.build()
		);

		InvoiceV1Controller.InvoicePayload invoiceResponse = given().
				standaloneSetup(new InvoiceV1Controller(invoiceService)).
				contentType("application/json").
				body(invoice).
				when().
				post("/v1/invoices").
				as(InvoiceV1Controller.InvoicePayload.class);

		assertThat(invoiceResponse.id).isEqualTo(1);
		assertThat(invoiceResponse.amountCents).isEqualTo(invoice.amountCents);
		assertThat(invoiceResponse.number).isEqualTo(invoice.number);
		assertThat(invoiceResponse.purchaseOrderNumber).isEqualTo(invoice.purchaseOrderNumber);
		assertThat(invoiceResponse.dueDate).isEqualTo(invoice.dueDate);
		assertThat(invoiceResponse.createdAt).isNotNull();
	}

	@Test
	public void invoice_search_invoice_number_empty() {
		when(invoiceService.findByInvoiceNumber(any(Pageable.class), anyString())).thenReturn(new PageImpl<>(Collections.emptyList()));

		given().
				standaloneSetup(new InvoiceV1Controller(invoiceService)).
				param("invoice_number", "aaa").
				when().
				get("/v1/invoices").
				then().
				statusCode(HttpStatus.OK.value()).
				contentType(MediaType.APPLICATION_JSON_VALUE).
				body("$", Matchers.empty());
	}

	@Test
	public void invoice_search_invoice_number_not_empty() throws Exception {
		Pageable page = new OffsetBasedPageable(0, MAX_PAGE_SIZE, DEFAULT_SORT);
		String invoiceNumber = "ABCDE12345";
		when(invoiceService.findByInvoiceNumber(page, invoiceNumber)).thenReturn(new PageImpl<>(
				Lists.newArrayList(Invoice.builder()
						.id(1L)
						.dueDate(new SimpleDateFormat("YYYY-MM-DD").parse("2018-01-30"))
						.invoiceNumber(invoiceNumber)
						.purchaseOrderNumber("X1X1X1")
						.amountCents(12345)
						.createdAt(new SimpleDateFormat("YYYY-MM-DD").parse("2018-02-22"))
						.build())
		));

		given().
				standaloneSetup(new InvoiceV1Controller(invoiceService)).
				param("invoice_number", invoiceNumber).
				when().
				get("/v1/invoices").
				then().
				statusCode(HttpStatus.OK.value()).
				contentType(MediaType.APPLICATION_JSON_VALUE).
				body("$", Matchers.hasSize(1));
	}

	@Test
	public void invoice_search_po_number_no_page() throws Exception {
		Pageable page = new OffsetBasedPageable(0, MAX_PAGE_SIZE, DEFAULT_SORT);
		String poNumber = "X1X1X1";
		when(invoiceService.findByPurchaseOrderNumber(page, poNumber)).thenReturn(new PageImpl<>(
				Lists.newArrayList(Invoice.builder()
						.id(1L)
						.dueDate(new SimpleDateFormat("YYYY-MM-DD").parse("2018-01-30"))
						.invoiceNumber("ABCDE12345")
						.purchaseOrderNumber(poNumber)
						.amountCents(12345)
						.createdAt(new SimpleDateFormat("YYYY-MM-DD").parse("2018-02-22"))
						.build())
		));

		given().
				standaloneSetup(new InvoiceV1Controller(invoiceService)).
				param("po_number", poNumber).
				when().
				get("/v1/invoices").
				then().
				statusCode(HttpStatus.OK.value()).
				contentType(MediaType.APPLICATION_JSON_VALUE).
				body("$", Matchers.hasSize(1));
	}

	@Test
	public void invoice_search_invoice_number_too_many() {
		Page<Invoice> result = mock(Page.class);
		when(result.hasNext()).thenReturn(true);
		when(result.getContent()).thenReturn(Collections.emptyList());
		when(invoiceService.findByInvoiceNumber(any(Pageable.class), anyString())).thenReturn(result);

		given().
				standaloneSetup(new InvoiceV1Controller(invoiceService)).
				param("invoice_number", "ABC").
				when().
				get("/v1/invoices").
				then().
				statusCode(HttpStatus.PARTIAL_CONTENT.value()).
				contentType(MediaType.APPLICATION_JSON_VALUE);
	}
}
