package invoice.service;

import javax.transaction.Transactional;

import lombok.RequiredArgsConstructor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import invoice.persistence.Invoice;
import invoice.persistence.InvoiceDao;

@Component
@RequiredArgsConstructor
public class InvoiceServiceImpl implements InvoiceService {
	private final InvoiceDao invoiceDao;

	@Transactional
	public Page<Invoice> findInvoice(Pageable pageable) {
		return invoiceDao.findAll(pageable);
	}

	@Transactional
	public Page<Invoice> findByInvoiceNumber(Pageable pageable, String invoiceNumber) {
		return invoiceDao.findByInvoiceNumber(pageable, invoiceNumber);
	}

	@Transactional
	public Page<Invoice> findByPurchaseOrderNumber(Pageable pageable, String purchaseOrderNumber) {
		return invoiceDao.findByPurchaseOrderNumber(pageable, purchaseOrderNumber);
	}

	@Transactional
	public Invoice createInvoice(Invoice invoice) {
		return invoiceDao.save(invoice);
	}
}
