package invoice.service;


import lombok.Data;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

/**
 * From https://stackoverflow.com/questions/25008472/pagination-in-spring-data-jpa-limit-and-offset
 */

@Data
public class OffsetBasedPageable implements Pageable {
	private int limit;
	private long offset;
	private final Sort sort;

	/**
	 * Creates a new {@link OffsetBasedPageable} with sort parameters applied.
	 *
	 * @param offset zero-based offset.
	 * @param limit  the size of the elements to be returned.
	 * @param sort   can be {@literal null}.
	 */
	public OffsetBasedPageable(long offset, int limit, Sort sort) {
		if (offset < 0) {
			throw new IllegalArgumentException("Offset index must not be less than zero!");
		}

		if (limit < 1) {
			throw new IllegalArgumentException("Limit must not be less than one!");
		}
		this.limit = limit;
		this.offset = offset;
		this.sort = sort;
	}

	@Override
	public int getPageNumber() {
		return (int)(offset / limit);
	}

	@Override
	public int getPageSize() {
		return limit;
	}

	@Override
	public Pageable next() {
		return new OffsetBasedPageable(getOffset() + getPageSize(), getPageSize(), getSort());
	}

	private OffsetBasedPageable previous() {
		return hasPrevious() ? new OffsetBasedPageable(getOffset() - getPageSize(), getPageSize(), getSort()) : this;
	}

	@Override
	public Pageable previousOrFirst() {
		return hasPrevious() ? previous() : first();
	}

	@Override
	public Pageable first() {
		return new OffsetBasedPageable(0, getPageSize(), getSort());
	}

	@Override
	public boolean hasPrevious() {
		return offset > limit;
	}
}
