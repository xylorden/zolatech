package invoice.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import invoice.persistence.Invoice;

public interface InvoiceService {
	Page<Invoice> findInvoice(Pageable pageable);

	Page<Invoice> findByInvoiceNumber(Pageable pageable, String invoiceNumber);

	Page<Invoice> findByPurchaseOrderNumber(Pageable pageable, String purchaseOrderNumber);

	Invoice createInvoice(Invoice invoice);
}
