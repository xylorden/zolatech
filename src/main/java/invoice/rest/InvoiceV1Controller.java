package invoice.rest;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Strings;
import invoice.persistence.Invoice;
import invoice.service.InvoiceService;
import invoice.service.OffsetBasedPageable;

@Slf4j
@RestController("InvoiceV1Controller")
@RequestMapping(path = "/v1/invoices", produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class InvoiceV1Controller {
	final static int MAX_PAGE_SIZE = 1000;
	final static Sort DEFAULT_SORT = new Sort(Sort.Direction.DESC, "CreatedAt");

	private final InvoiceService invoiceService;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<List<InvoicePayload>> searchInvoices(@RequestParam(value = "invoice_number", required = false) String invoiceNumber,
															   @RequestParam(value = "po_number", required = false) String purchaseOrderNumber,
															   @RequestParam(value = "offset", required = false) Long offset,
															   @RequestParam(value = "limit", required = false) Integer limit) {

		if (!Strings.isNullOrEmpty(invoiceNumber) && !Strings.isNullOrEmpty(purchaseOrderNumber)) {
			throw new IllegalArgumentException("Can only search invoice by either invoice_number or po_number");
		}

		OffsetBasedPageable pageable = new OffsetBasedPageable(offset == null ? 0 : offset, getPageSize(limit), DEFAULT_SORT);

		Page<Invoice> invoices = new PageImpl<>(Collections.emptyList());
		if (Strings.isNullOrEmpty(invoiceNumber) && Strings.isNullOrEmpty(purchaseOrderNumber)) {
			invoices = invoiceService.findInvoice(pageable);
		}

		if (!Strings.isNullOrEmpty(invoiceNumber)) {
			invoices = invoiceService.findByInvoiceNumber(pageable, invoiceNumber);
		}

		if (!Strings.isNullOrEmpty(purchaseOrderNumber)) {
			invoices = invoiceService.findByPurchaseOrderNumber(pageable, purchaseOrderNumber);
		}

		return new ResponseEntity<>(
				invoices.getContent().stream()
						.map(invoice -> new InvoicePayload(invoice.getId(), invoice.getInvoiceNumber(), invoice.getPurchaseOrderNumber(), invoice.getDueDate(), invoice.getAmountCents(), invoice.getCreatedAt()))
						.collect(Collectors.toList()),
				invoices.hasNext() ? HttpStatus.PARTIAL_CONTENT : HttpStatus.OK
		);
	}

	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public InvoicePayload createInvoice(@Valid @RequestBody InvoicePayload invoicePayload) {
		if (invoicePayload == null) {
			throw new IllegalArgumentException("Empty payload received.");
		}

		Invoice createdInvoice = invoiceService.createInvoice(invoice.persistence.Invoice.builder()
				.invoiceNumber(invoicePayload.number)
				.purchaseOrderNumber(invoicePayload.purchaseOrderNumber)
				.amountCents(invoicePayload.amountCents)
				.dueDate(invoicePayload.dueDate)
				.build());

		return new InvoicePayload(createdInvoice.getId(), createdInvoice.getInvoiceNumber(), createdInvoice.getPurchaseOrderNumber(), createdInvoice.getDueDate(), createdInvoice.getAmountCents(), createdInvoice.getCreatedAt());
	}

	/**
	 * logic to set page size to avoid any performance problem that could happen if requester specify a too big limit
	 * @param requested requested limit
	 * @return MAX_PAGE_SIZE is no limit is specified, or if limit is greater that arbitrary value
	 */
	private int getPageSize(Integer requested) {
		if (requested == null) {
			return MAX_PAGE_SIZE;
		}
		if (requested > MAX_PAGE_SIZE) {
			return MAX_PAGE_SIZE;
		}
		return requested;
	}

	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	static class InvoicePayload {
		@JsonProperty("id")
		long id;

		@JsonProperty("invoice_number")
		@NotNull
		@Size(min = 1, max = 64, message = "number out of range.")
		String number;

		@JsonProperty("po_number")
		@NotNull
		@Size(min = 1, max = 64, message = "po_number out of range.")
		String purchaseOrderNumber;

		@JsonProperty("due_date")
		@NotNull
		Date dueDate;

		@JsonProperty("amount_cents")
		@NotNull
		long amountCents;

		@JsonProperty("created_at")
		@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
		Date createdAt;
	}
}
