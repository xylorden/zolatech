package invoice.persistence;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InvoiceDao extends JpaRepository<Invoice, Long> {
	Page<Invoice> findByInvoiceNumber(Pageable pageable, String invoiceNumber);

	Page<Invoice> findByPurchaseOrderNumber(Pageable pageable, String purchaseOrderNumber);
}
