package invoice.persistence;

import static javax.persistence.TemporalType.TIMESTAMP;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "invoices")
public class Invoice {
	@Id
	@GeneratedValue
	@Column(name = "Id", nullable = false)
	private Long id;

	@Temporal(TIMESTAMP)
	@Column(name = "created_at", nullable = false)
	private Date createdAt;

	@Column(name = "invoice_number", length = 64, nullable = false)
	private String invoiceNumber;

	@Column(name = "po_number", length = 64, nullable = false)
	private String purchaseOrderNumber;

	@Column(name = "amount_cents", nullable = false)
	private long amountCents;

	@Temporal(TemporalType.DATE)
	@Column(name = "due_Date", nullable = false)
	private Date dueDate;

	@PrePersist
	public void prePersist() {
		createdAt = new Date();
	}
}
