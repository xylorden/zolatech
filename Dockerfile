FROM openjdk:8u181-jre-slim

RUN mkdir /h2

VOLUME /h2

ADD target/zolatech.jar zolatech.jar

ENTRYPOINT [ "sh", "-c", "exec java -server -jar /zolatech.jar" ]
